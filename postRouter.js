var express=require("express")
const multer=require("multer");
const json = require("body-parser/lib/types/json")
const bodyparser=require("body-parser")
const post=require("./post")
var router=express.Router()
const cors=require("cors");
const path=require("path")

const corsOptions ={
    origin:'*', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200,
 }

router.use(cors(corsOptions))

router.use(express.json({limit:'1mb'}))

router.use(bodyparser.urlencoded({
    extended:true
}))

router.use(express.static('public'));

//storing image
var pa="";
var extension="";
var original=""
const storage=multer.diskStorage({
    //where it will store
    destination:"./public/posts",
    //file info
    filename:(req,file,cb)=>{
        original=file
        console.log("file====================>");
        let name="post"+Date.now()+""
        cb(null,name+path.extname(file.originalname))
        extension=path.extname(file.originalname);
        pa="/posts/"+name+path.extname(file.originalname)
    }
})
//create an instance for upload an image
const upload=multer({storage:storage})
console.log("/post");
router.post("/uploads",upload.single('post'),(req,res)=>{
    if(pa==""){
        extension=""
    }
    console.log("-----------------------------------> ",extension);
    let result=post.postData(req,pa,extension,original)
    original=""
    extension=""
    if(result!=false){
        res.json({
            status:"success",
        }).status(201)
    }else if(result=="WFR"){
        res.json({
            status:"retry",
            uid:req.body.uid
        }).status(204)
    }
    else{
        res.json({
            status:"failed",
            uid:req.body.uid
        }).status(204)
    }
})
router.post("/fetch",async(req,res)=>{
    console.log("/fetch");
    let result=await post.loadData()
    let likes=await post.getLikes()
    let likeCount=await result["likeCount"]
   
    console.log(result["status"]);
    if(result["status"]=="success"){
        return res.json({
            result,
            likes,
            likeCount
        })
    }else{
        return res.json({
            status:"failed",
            message:"i think no data!"
        })
    }
})

router.post("/like",async(req,res)=>{
    if(post.likePost(req)){
        console.log("post liked");
        res.json({
            status:"success",
        }).status(201)
    } else{
        res.json({
            status:"failed",
        }).status(204)
    }
})

module.exports=router;