const sql= require("sync-sql")
const lib=require("./lib")
var fs = require('fs');
const { stat } = require("sync");

var config ={
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'mark10'
  }

  function postData(req,posts,ptype,post_name){
      if(post_name==""){
          post_name=""
      }
    let extension=0;  
    if(ptype==".jpeg"||ptype==".png"||ptype==".jpg"||ptype==".svg"){
        extension=2;
      }else if(ptype==".mkv" || ptype==".mp4" || ptype==".avi"){
          extension=1;
      }else if(ptype==".txt" || ptype==".pdf" || ptype==".docx"){
          extension=4
      }else if(ptype==""){
          extension=5
      }
      else{
          extension=0;
      }
      if(extension!=0){
    let pid="postid_"+(lib.getUid())+"_mark10";
    let query=`insert into post(uid,pid,posts,ptype,pcaption,ptime,username,post_name) values("${req.body["uid"]}","${pid}","${posts}","${extension}","${req.body.caption}",CURDATE(),"${req.body["name"]}","${post_name.originalname}")`;
    var res=sql.mysql(config,query)
    if(res.data.rows["affectedRows"]>=1){
        return res
    }else{
        return false
    }
}else{
    console.log("file deleted");
    var filePath ="public/"+posts
    fs.unlinkSync(filePath);
    return "WFR";
}
  }

  async function loadData() {
      
      var query=`select * from post order by sno desc`;
      var result=sql.mysql(config,query)
     
      if(result.data.rows.length>=1){
        let like_count=[]
        let temp;
        result.data.rows.forEach(async(item,index)=>{
            temp=await getLikesCount(item["pid"])
            like_count.push(temp)
        })
          return {
              status:"success",
              data:result.data.rows,
              likeCount:like_count,
            }
    }
      else{
          return{
              status:"failed",
              message:"no data"
          }
      }
  }


  async function likePost(req) {
      let state=req.body.state;
      var query=`insert into likes(pid,uid,time)values("${req.body.pid}","${req.body.uid}",CURRENT_TIMESTAMP())`;
      if(state=="unlike"){
        var query=`delete from likes where uid="${req.body.uid}" and pid="${req.body.pid}"`;
        console.log(query);
      }
      var res=sql.mysql(config,query)
      if(res.data.rows["affectedRows"]>=1){
        return res
    }else{
        return false
    }
  }

  async function getLikes() {
    var query=`select * from likes`;
    var result=sql.mysql(config,query)
   
    if(result.data.rows.length>=1){
        let deer=[result.data.rows]
        result.data.rows.forEach((item,index)=>{
            console.log(item["pid"]);
        })
        return {
            status:"success",
            data:result.data.rows,
          }
  }
    else{
        return{
            status:"failed",
            message:"no data"
        }
    }
  }

  async function getLikesCount(pid) {
    //var query=`select pid,count(*) from likes group by pid`;
    var query=`select count(*) as count from likes where pid="${pid}"`;
    var result=sql.mysql(config,query)
    if(result.data.rows.length>=1){
       
        return result.data.rows[0]["count"]
  }
    else{
        return 0
    } 
  }

  var fun={
      postData:postData,
      loadData:loadData,
      likePost:likePost,
      getLikes:getLikes,
      getLikesCount:getLikesCount,
  }

  module.exports=fun;