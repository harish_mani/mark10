const { get } = require("lodash");
const sql= require("sync-sql")
const lib=require("./lib")

var config ={
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'mark10'
  }

function updateProfile(id,req) {
    let query=`insert into profiles values("${id}","${req.body.name}",DEFAULT,"${req.body.email}",DEFAULT,DEFAULT)`;
    var res=sql.mysql(config,query)
    console.log(res);
    if(res.data.rows.length==1){
        return res
    }else{
        return 0
    }
}

function insertData(req) {
    id=lib.getUid();
    var password=lib.hashPassword(req.body.password)
    let query=`insert into users values("${id}","${req.body.name}","${req.body.email}","${password}",${0})`;
    var res=sql.mysql(config,query)
    if(res["success"]==true){
        let otp=lib.sendMail(req);
        query=`insert into verify(id,otp)values("${id}","${otp}")`;
        let result=sql.mysql(config,query)
        updateProfile(id,req)
        return {status:"success",otp:otp,id:id}
    }else{
        return {status:"failed",id:"NoId"}
    }
}

function getOtp(id) {
    let query=`select otp from verify where id="${id}" order by sno desc limit 1`;
    var res=sql.mysql(config,query)
    if(res.data.rows.length==1){
        return res
    }else{
        return "NoOtp"
    }
}
function activateAccount(id) {
    let query=`update users set active=1 where uid="${id}"`;
    var res=sql.mysql(config,query)
    if(res.data.rows["affectedRows"]==1){
        const date = new Date();
        date.setDate(date.getDate()+1);
        var expiry=date.toISOString().split('T')[0]
        let token=lib.hashPassword(lib.getUid())
        addSession(id,token,expiry)
        return {
            status:"success",
            id:id,
            expiry:expiry,
            token:token
        }
    }else{
        return false
    } 
}
function login(req,respo) {
    let query=`select * from users where email="${req.body.email}" and password="${lib.hashPassword(req.body.password)}"`;
    var res=sql.mysql(config,query)
    if(res.data.rows.length==1){
        const date = new Date();
        date.setDate(date.getDate()+1);
        var expiry=date.toISOString().split('T')[0]
        let token=lib.hashPassword(lib.getUid())
        if(addSession(res.data.rows[0]["uid"],token,expiry)){
            return {uid:res.data.rows[0]["uid"],
            token:token,
            expiry:expiry}
        }else{
            return "noToken"
        }
    }else{
        return "NoId"
    }
}

function addSession(uid,token,expiry) {
    let query=`insert into session (uid,token,expiry,state)values("${uid}","${token}","${expiry}",${1})`;
    var res=sql.mysql(config,query)
    if(res["success"]==true){
        return true
    }else{
        return false
    }
}

function validate(req) {
    let query=`select * from session where uid="${req.body.uid}" order by sno desc limit 1`;
    var res=sql.mysql(config,query)
    console.log("quaery ",query);
    if(res.data.rows.length==1){
        console.log("ulla1");
    let date=new Date()
    let db_date=new Date(res.data.rows[0]["expiry"]);    
    let current_date=new Date(date.toISOString());
    if(db_date>current_date){
        console.log("ulla2");  
        return res
    }else{
        console.log("veliya1");
        logout(req.body.uid)
        return"NoUser"
    }
    }else{
        console.log("veliya2");
        return "NoUser"
    }
}

function fetchData(req) {
    console.log(req.body);
    let query=`select * from profiles where uid="${req.body.uid.split("=")[1]}"`;
    var res=sql.mysql(config,query)  
    console.log(res.data.rows);

    if(res.data.rows.length==1){
        return res
    }else{
        return "NoUser"
    }
}

function updateData(req) {
    let query=`update profiles set name="${req.body.name}",email="${req.body.email}",location="${req.body.location}",
                bio="${req.body.bio}",lastname="${req.body.lastname}" where uid="${req.body.uid}"`;
    var res=sql.mysql(config,query)
    if(res.data.rows["affectedRows"]==1){
        query=`update users set name="${req.body.name}",email="${req.body.email}"  where uid="${req.body.uid}"`;
        res=sql.mysql(config,query)
        if(res.data.rows["affectedRows"]==1){
            return true
        }else{
            return false;
        }
    }else{
        return false
    } 
}

function updateProfilePics(req,path) {
    let query=`insert into propics (uid,image)values("${req.body.uid}","${path}")`;
    var res=sql.mysql(config,query)
    if(res["success"]==true){
        return true
    }else{
        return false
    }
}

function getProfile(uid) {
    let query=`select image from propics where uid="${uid.split("=")[1]}" order by sno desc limit 1`;
    var res=sql.mysql(config,query)  
    if(res.data.rows.length==1){
        return res.data.rows[0]["image"]
    }else{
        return false
    }
}

function logout(req) {
    console.log("body ",req);
    let query=`update session set state=0 where uid="${req.body.uid}"`;
    var res=sql.mysql(config,query)
    console.log(res);
    if(res.data.rows["affectedRows"]>=1){
        return true
    }else{
        return false
    }
}

var fun={
    insertData:insertData,
    getOtp:getOtp,
    activateAccount:activateAccount,
    login:login,
    validate:validate,
    addSession:addSession,
    fetchData:fetchData,
    updateData:updateData,
    updateProfilePics:updateProfilePics,
    getProfile:getProfile,
    logout:logout
}
module.exports=fun;
