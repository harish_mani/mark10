const express=require("express")
const bodyparser=require("body-parser")
const cookies=require("cookie-parser")
const db=require("./db")
const lib4=require("./lib")
const { lib } = require("crypto-js");
const {v4:uuidv4}=require("uuid")
const path=require("path");
const { log } = require("console");
const multer=require("multer");
const app=express()

const router=require("./postRouter")

const cors=require("cors");
const json = require("body-parser/lib/types/json")


const corsOptions ={
    origin:'*', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200,
 }
 
 //post divertion
app.use("/post",router)

 app.use(cors(corsOptions)) // Use this after the variable declaration
 app.use(cookies())

app.use(express.json({limit:'1mb'}))

// app.set('view engine','ejs')
app.set('views',__dirname+'/views')

app.use(express.json({limit:'1mb'}))

app.use(bodyparser.urlencoded({
    extended:true
}))

app.use(express.static("public"))

var pa="";
//storing image
const storage=multer.diskStorage({
    //where it will store
    destination:(req,file,cb)=>{
        cb(null,'public/users_image')
    },
    //file info
    filename:(req,file,cb)=>{
        let name=Date.now()+""
        cb(null,name+path.extname(file.originalname))
        pa="users_image/"+name+path.extname(file.originalname)
    }
})
//create an instance for upload an image
const upload=multer({storage:storage})
let i=0
app.post("/profile",upload.single('file1'),(req,res)=>{
    console.log(pa);
    if(i<3){
    if(db.updateProfilePics(req,pa)){
        res.json({
            status:"success",
            uid:req.body.uid
        }).status(201)
    }else{
        res.json({
            status:"failed",
        }).status(204)
    }
    i=i+1;
}else{
    res.json({
        status:"retry",
    }).status(204)
}
})

app.post("/",(req,res)=>{
    i=0
    console.log("data",req.body);
    if(req.body==undefined){
        res.json({
            status:"Failed",
            message:"Please Login"
        }).status(204)
    }else{
    console.log("validate");
    var result=db.validate(req)
    console.log(result);
    if(result!="NoUser"){

        if(result.data.rows[0]["token"]==req.body.token){
            res.json({
                status:"success",
                uid:result.data.rows[0]["uid"],
            }).status(201)
        }else{
            res.json({
                status:"Failed",
                message:"Session failed"
            }).status(204)
        }
    }else{
        res.json({
            status:"Failed",
            message:"Session closed"
        }).status(204)
    }
}
})

app.post("/fetch",(req,res)=>{
    console.log("fetching...");
    var result=db.fetchData(req)
    
    let image=db.getProfile(req.body.uid)
   
    if(result!="NoUser"){
        return res.json({
            status:"success",
            uid:result.data.rows[0]["uid"],
            name:result.data.rows[0]["name"],
            lastname:result.data.rows[0]["lastname"],
            email:result.data.rows[0]["email"],
            bio:result.data.rows[0]["bio"],
            location:result.data.rows[0]["location"],
            image:image
        }).status(201)
    }else{
        res.json({
            status:"Failed",
            message:"Session closed"
        }).status(204)
    
    }
})

app.post("/signin",(req,res)=>{
    let result=db.login(req,res)
    if(result!="NoId"){
        res.json({
            status:"success",
            uid:result["uid"],
            token:result["token"],
            expiry:result["expiry"]
        }).status(201)
    }else if(result=="noToken"){
        res.json({
            status:"failed",
            message:"session"
        }).status(204)
    }
    else{
        res.json({
            status:"failed",
            message:"wrong validation"
        }).status(204)
    }
})

app.post("/signup",(req,res)=>{
    let result=db.insertData(req)
    otp=result["otp"]
    id=result["id"]
    if(result["status"]=="success"){
        res.json({
            status:"success",
            otp:otp,
            id:id 
        }).status(201)
    }else{
        res.json({
            status:"failed",
            message:"insertion failed"
        })
    }
})

app.post("/verify",(req,res)=>{
    console.log("activate mode");
    let otp=db.getOtp(req.body.id)
    if(otp.data.rows[0]["otp"]==lib4.hashPassword(req.body.otp)){
        let result=db.activateAccount(req.body.id)
        console.log(result);
        res.json({
            status:"success",
            uid:req.body.id,
            token:result["token"],
            expiry:result["expiry"]
        }).status(201)
        
    }else{
        res.json({
            status:"failed",
            message:"wrong otp"
        }).status(204)
    }
})

app.post("/account/update",(req,res)=>{
    
    if(db.updateData(req)){
         res.json({
            status:"success",
            message:"updated"
        }).status(20)
    }else{
        res.json({
            status:"failed",
            message:"update failed"
        }).status(204) 
    }
})

app.post("/logout",(req,res)=>{
    console.log("logout");
    if(db.logout(req)){
        res.json({
            status:"success",
            message:"logout"
        }).status(20)
    }else{
        res.json({
            status:"failed",
        }).status(204) 
    }
})

app.listen(4444,()=>{
    console.log("Listening on port 4444");
});