const view_signup=document.querySelector(".signup")
const view_login=document.querySelector(".login")

const change_signup=document.querySelector("#signup-button");
const change_login=document.querySelector("#login-button");

change_signup.addEventListener("click",changeIntoSignup)
change_login.addEventListener("click",changeIntoLogin)

function changeIntoSignup() {
    view_login.style.display="none";
    view_signup.style.display="flex";
    change_signup.style.backgroundColor="white";
    change_signup.style.color="black";
    change_signup.style.fontSize="18px";
    change_login.style.backgroundColor="#E96499";
    change_login.style.color="white";
    change_login.style.fontSize="12px";
}


function changeIntoLogin() {
    view_login.style.display="flex";
    view_signup.style.display="none";
    change_login.style.backgroundColor="white";
    change_login.style.color="black";
    change_login.style.fontSize="18px";
    change_signup.style.backgroundColor="#E96499";
    change_signup.style.color="white";
    change_signup.style.fontSize="12px";
}